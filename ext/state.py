# -*- coding: utf-8 -*-

import asyncio
import logging

import asyncpg
import discord
from discord.ext import commands

from .common import Cog
from .utils import Timer
from jose import JoseBot

log = logging.getLogger(__name__)


class State(Cog, requires=["config"]):
    """Synchronizes José's state to the PostgreSQL database."""

    def __init__(self, bot: JoseBot):
        super().__init__(bot)
        self.loop.create_task(self.full_sync())

    @property
    def db(self) -> asyncpg.pool.Pool:
        cfg = self.bot.get_cog("Config")
        if cfg is not None:
            return cfg.db
        raise RuntimeError("Required config Cog is not loaded.")

    # NOTE: on_member_join and on_member_remove require the members intent,
    # which I do not have. We will be desyncced from the truth as time passes,
    # but I don't give a fuck.
    #
    # We try our best using MESSAGE_CREATE and MEMBER_BAN, but we'll be
    # always behind the actual member list.
    #
    # Future improvements would be periodically syncing guild state using the
    # HTTP API. But basing off the existing events we have is better than
    # waiting for 600 guilds to sync up manually.
    @Cog.listener()
    async def on_member_join(self, member: discord.Member):
        await self.insert_member_to_db(member.guild.id, member.id)

    @Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        await self.remove_member_from_db(member.guild.id, member.id)

    @Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        await self.sync_guild(guild)
        log.info(f"synced state of {guild} {guild.id} (join)")

    @Cog.listener()
    async def on_guild_available(self, guild: discord.Guild):
        await self.sync_guild(guild)
        log.info(f"synced state of {guild} {guild.id} (available)")

    @Cog.listener()
    async def on_guild_remove(self, guild: discord.Guild):
        # When being removed from a guild, we shouldn't keep its data up.
        # We are good data citizens.
        #
        # If they add the bot back some time later, we won't be able to provide
        # full service, but oh well. such is life.
        async with self.db.acquire() as conn:
            await conn.execute("DELETE FROM members WHERE guild_id = $1", guild.id)

    async def insert_user_to_db(self, user: discord.User, conn=None) -> None:
        conn = conn or self.db
        await conn.execute(
            """
            INSERT INTO users (id, name, discriminator, avatar, bot)
            VALUES ($1, $2, $3, $4, $5)

            ON CONFLICT ON CONSTRAINT users_pkey
            DO UPDATE SET
                name = $2,
                discriminator = $3,
                avatar = $4,
                bot = $5
            WHERE users.id = $1
            """,
            user.id,
            user.name,
            user.discriminator,
            user.avatar,
            user.bot,
        )

    async def insert_member_to_db(self, guild_id: int, user_id: int) -> None:
        await self.db.execute(
            """
            INSERT INTO members
                (guild_id, user_id)
            VALUES
                ($1, $2)
            ON CONFLICT ON CONSTRAINT members_pkey
            DO NOTHING
            """,
            guild_id,
            user_id,
        )

    async def remove_member_from_db(self, guild_id: int, user_id: int) -> None:
        await self.db.execute(
            """
            DELETE FROM members
            WHERE guild_id = $1 AND user_id = $2
            """,
            guild_id,
            user_id,
        )

    @Cog.listener()
    async def on_message(self, message: discord.Message):
        # Message.author will always exist. The existing discord.py cache
        # does add the author to the cache, but we lose it upon restarts.
        #
        # the plan here is to save the author data (id, username, discrim) on
        # a table, and cache it on the bot after lookups.
        #
        # should aid the discord.User argument converter to find more users
        # via their names.
        author = message.author
        await self.insert_user_to_db(author)

        # if the message is coming from a guild, we can assume the author
        # is a member of the guild.
        if message.guild:
            await self.insert_member_to_db(message.guild.id, author.id)

    @Cog.listener()
    async def on_member_ban(self, guild, user):
        # if we saw someone being banned, we shouldn't keep the member link up.
        #
        # we don't have the members intent, so we can't rely on the
        # on_member_remove event.
        await self.remove_member_from_db(guild.id, user.id)

    async def full_sync(self):
        """Attempt to sync with all the guilds."""
        await self.bot.wait_until_ready()

        log.info("starting to sync state")

        # since the connection pool has 10 connections we might as well use them
        with Timer() as timer:
            await asyncio.gather(*[self.sync_guild(x) for x in self.bot.guilds])

        log.info(f"synced full state, took {timer}")

    async def sync_guild(self, guild: discord.Guild):
        """Attempts to 'sync' up a guild's member list to the members table in a
        best-effort way."""
        async with self.db.acquire() as conn:
            new_members = []
            new_users = []

            # use all the cache we have
            for member in guild.members:
                new_members.append((guild.id, member.id))
                new_users.append(
                    (
                        member.id,
                        member.name,
                        member.discriminator,
                        member.avatar,
                        member.bot,
                    )
                )

            await conn.executemany(
                """
                INSERT INTO members
                    (guild_id, user_id)
                VALUES
                    ($1, $2)
                ON CONFLICT ON CONSTRAINT members_pkey
                DO NOTHING
                """,
                new_members,
            )

            await conn.executemany(
                """
                INSERT INTO users (id, name, discriminator, avatar, bot)
                VALUES ($1, $2, $3, $4, $5)

                ON CONFLICT ON CONSTRAINT users_pkey
                DO UPDATE SET
                    name = $2,
                    discriminator = $3,
                    avatar = $4,
                    bot = $5
                WHERE users.id = $1
                """,
                new_users,
            )

    @commands.command()
    @commands.is_owner()
    async def forcefill(self, ctx):
        """Force filling data from all members in the members table to the
        users table."""
        total, success = 0, 0
        async with self.db.acquire() as conn:
            async with conn.transaction():
                async for record in conn.cursor(
                    "SELECT DISTINCT(user_id) FROM members"
                ):
                    total += 1
                    user = self.bot.get_user(record["user_id"])
                    if user is not None:
                        success += 1
                        await self.insert_user_to_db(user, conn=conn)

        await ctx.send(f"members has {total} users, backfilled {success} users")


def setup(bot: JoseBot):
    bot.add_cog(State(bot))
