from collections import namedtuple

import discord
from discord.ext import commands
from dns import resolver, rdatatype

from .common import Cog, SayException

CABAL_ID = 720020173310132348


class BBL(Cog):
    def __init__(self, bot):
        super().__init__(bot)

    def cog_check(self, ctx):
        if not ctx.guild:
            return False

        return ctx.guild.id == CABAL_ID

    @commands.group(invoke_without_command=True)
    async def bbl(self, ctx):
        """This command is only usable in a specific guild."""
        pass

    @bbl.command(name="set")
    async def bbl_set(self, ctx, project: str, provider: str, server_count: int):
        """Set your server counts for a project"""

        project = project.lower()
        provider = provider.lower()

        await self.pool.execute(
            """
            INSERT INTO bbl_stats
                (user_id, project, provider, server_count)
            VALUES
                ($1, $2, $3, $4)
            ON CONFLICT ON CONSTRAINT bbl_stats_pkey DO
            UPDATE
                SET server_count = $4
                WHERE bbl_stats.user_id = $1
                  AND bbl_stats.project = $2
                  AND bbl_stats.provider = $3
            """,
            ctx.author.id,
            project,
            provider,
            server_count,
        )

        await ctx.ok()

    @bbl.command(name="rm")
    async def bbl_remove(self, ctx, project: str, provider: str):
        """Remove your provider fleet from a project"""

        project = project.lower()
        provider = provider.lower()

        await self.pool.execute(
            """
            DELETE FROM bbl_stats
            WHERE user_id = $1
              AND project = $2
              AND provider = $3

            """,
            ctx.author.id,
            project,
            provider,
        )

        await ctx.ok()

    @bbl.command(name="get")
    async def bbl_get(self, ctx, project: str):
        """Get total count for a project."""

        em = discord.Embed(description="")
        rows = await self.pool.fetch(
            """
            SELECT user_id, provider, server_count FROM bbl_stats
            WHERE project = $1
            """,
            project,
        )

        total = 0
        for row in rows:
            user_id = row["user_id"]
            server_count = row["server_count"]
            provider = row["provider"]

            em.description += f"<@{user_id}> has {server_count} in provider {provider}"
            total += server_count

        em.set_footer(text=f"{total} total servers")
        print(em)
        await ctx.send(embed=em)


def setup(bot):
    bot.add_jose_cog(BBL)
